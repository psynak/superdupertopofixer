/* VertexProperties.h
 *
 * Peter Synak, Chris Wojtan, Huidong Yang, Aleksei Kalinov, Malina Strugaru, Arian Etemadi 2023
 *
 * This is the header for the vertex properties struct.
 */

#pragma once

//------------------------------------------------------------
// includes
//------------------------------------------------------------

#include "../utilities/vec.h"

//------------------------------------------------------------
// classes
//------------------------------------------------------------

// struct of properties that are stored per vertex of the mesh,
// including velocity, thickness, derivative of thickness, and surface flow,
// two of which are double values and two Vec3d values.

struct VertexProperties {
 public:
	// constructors
	VertexProperties();
	VertexProperties(Vec3d vel, Vec3d flow_v, double th_v, double uth_v);
	VertexProperties(const VertexProperties& vert_props);
	~VertexProperties();

	// returns a VertexProperties struct with linearly interpolated values
	const static VertexProperties linearInterpolation(const VertexProperties& p1,
													  const VertexProperties& p2,
													  double alpha);

	// getters and setters
	Vec3d getVelocity() const { return vector_props[0]; }
	Vec3d getFlowV() const { return vector_props[1]; }
	double getThickness() const { return scalar_props[0]; }
	double getUThickness() const { return scalar_props[1]; }
	void setVelocity(Vec3d vel) { vector_props[0] = vel; }
	void setFlowV(Vec3d flow_v) { vector_props[1] = flow_v; }
	void setThickness(double th_v) { scalar_props[0] = th_v; }
	void setUThickness(double uth_v) { scalar_props[1] = uth_v; }

	// functions
	void setZero();
	void iadd(const VertexProperties& other);
	void iadd(const VertexProperties& other, double scale);
	void idiv(double scale);
	void imul(double scale);
	VertexProperties div (double scale) const;

	// override operator>>, for reading lines with keyword v_props from .obj files
	friend std::istream& operator>>(std::istream& is, VertexProperties& vert_props) {
		for (int i = 0; i < numVectorProperties; i++) {
			is >> vert_props.vector_props[i][0];
			is >> vert_props.vector_props[i][1];
			is >> vert_props.vector_props[i][2];
		}
		for (int i = 0; i < numScalarProperties; i++) {
			is >> vert_props.scalar_props[i];
		}
		return is;
	}

	// override operator<<, for writing lines with keyword v_props in .obj files and debug prints
	friend std::ostream& operator<<(std::ostream& os, const VertexProperties& vert_props) {
		for (int i = 0; i < numVectorProperties; i++) {
			os << vert_props.vector_props[i] << ' ';
		}
		for (int i = 0; i < numScalarProperties; i++) {
			os << vert_props.scalar_props[i] << ' ';
		}
		return os;
	}

 private:
	static const int numScalarProperties = 2;
	static const int numVectorProperties = 2;

	// data structures
	Vec3d vector_props[numVectorProperties];
	double scalar_props[numScalarProperties];
};
