/* VertexProperties.cpp
 *
 * Peter Synak, Chris Wojtan, Huidong Yang, Aleksei Kalinov, Malina Strugaru, Arian Etemadi 2023
 *
 * This is the implementation file for the vertex properties struct.
 */

//------------------------------------------------------------
// includes
//------------------------------------------------------------

#include "VertexProperties.h"

//------------------------------------------------------------
// constructors
//------------------------------------------------------------

// default constructor
VertexProperties::VertexProperties() {
    setZero();
}

VertexProperties::VertexProperties(Vec3d vel, Vec3d flow_v, double th_v, double uth_v) {
    vector_props[0] = vel;
    vector_props[1] = flow_v;
    scalar_props[0] = th_v;
    scalar_props[1] = uth_v;
}

// copy constructor
VertexProperties::VertexProperties(const VertexProperties& vert_props) {
    for (int i = 0; i < numVectorProperties; i++) {
        vector_props[i] = vert_props.vector_props[i];
    }
    for (int i = 0; i < numScalarProperties; i++) {
        scalar_props[i] = vert_props.scalar_props[i];
    }
}

// default destructor
VertexProperties::~VertexProperties() {}

//------------------------------------------------------------
// functions
//------------------------------------------------------------

const VertexProperties VertexProperties::linearInterpolation(const VertexProperties& p1,
                                                             const VertexProperties& p2,
                                                             double alpha) {
    VertexProperties ret;
    for (int i = 0; i < numVectorProperties; i++) {
        ret.vector_props[i] = (1 - alpha) * p1.vector_props[i] + alpha * p2.vector_props[i];
    }
    for (int i = 0; i < numScalarProperties; i++) {
        ret.scalar_props[i] = (1 - alpha) * p1.scalar_props[i] + alpha * p2.scalar_props[i];
    }
    return ret;
}

void VertexProperties::setZero() {
    for (int i = 0; i < numVectorProperties; i++) {
        vector_props[i] = Vec3d(0);
    }
    for (int i = 0; i < numScalarProperties; i++) {
        scalar_props[i] = 0;
    }
}

void VertexProperties::iadd(const VertexProperties& other) {
    iadd(other, 1);
}

void VertexProperties::iadd(const VertexProperties& other, double scale) {
    for (int i = 0; i < numVectorProperties; i++) {
        vector_props[i] += other.vector_props[i] * scale;
    }
    for (int i = 0; i < numScalarProperties; i++) {
        scalar_props[i] += other.scalar_props[i] * scale;
    }
}

void VertexProperties::idiv(double scale) {
    for (Vec3d& vp : vector_props) {
        vp /= scale;
    }
    for (double& sp : scalar_props) {
        sp /= scale;
    }
}

void VertexProperties::imul(double scale) {
    for (Vec3d& vp : vector_props) {
        vp *= scale;
    }
    for (double& sp : scalar_props) {
        sp *= scale;
    }
}

VertexProperties VertexProperties::div(double scale) const {
    VertexProperties ret(*this);
    ret.idiv(scale);
    return ret;
}
