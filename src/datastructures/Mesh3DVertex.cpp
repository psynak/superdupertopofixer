/* Mesh3DVertex.cpp
 *
 * Peter Synak, Chris Wojtan, Huidong Yang, Aleksei Kalinov, Malina Strugaru, 2021
 *
 * This is the implementation file for the mesh vertex class.
 */

//------------------------------------------------------------
// includes
//------------------------------------------------------------

#include "Mesh3DVertex.h"
#include "Mesh3DHalfCorner.h"

//------------------------------------------------------------
// constructors
//------------------------------------------------------------

// default constructor
Mesh3DVertex::Mesh3DVertex() {}

// default destructor
Mesh3DVertex::~Mesh3DVertex() {}

//------------------------------------------------------------
// functions
//------------------------------------------------------------
