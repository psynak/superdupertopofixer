/* Grid3DCubical.cpp
 *
 * Peter Synak, Chris Wojtan, Huidong Yang, 2021
 *
 * This is the implementation file for the cubical grid implementation.
 */

//------------------------------------------------------------
// includes
//------------------------------------------------------------

#include "Grid3DCubical.h"

//------------------------------------------------------------
// constructors
//------------------------------------------------------------

// default constructor
Grid3DCubical::Grid3DCubical() {}

// default destructor
Grid3DCubical::~Grid3DCubical() {}

//------------------------------------------------------------
// functions
//------------------------------------------------------------