/* ModuleInterface.cpp
 *
 * Peter Synak, Chris Wojtan, Huidong Yang, 2021
 *
 * This is the implementation file for the module interface. Functions that are uniform over all
 * modules should be implemented here.
 */

//------------------------------------------------------------
// includes
//------------------------------------------------------------

#include "ModuleInterface.h"