# Stanford bunny input 

This directory inlcudes classic 3D model of Stanford bunny[1] and the scripts to preprocess the original model into format suitable for tracker. 

[1] "Zippered Polygon Meshes from Range Images"
Greg Turk and Marc Levoy
Siggraph 94, pp. 311-318

## Inlcuded files: 

* `bunny.obj`: unscalled and unmodified version without any materials attached to it. 
* `sunny_bunny.obj`: the prepared model ready for use in SuperDuperTopoFixer.

* `generate_materials.py`: a Python script converting the original bunny into a sunny one. 

