input_mesh_file testinputs/threeballoverlapping_corrected.obj

num_grid_cells 20
grid_bounding_box_style minmax_cube
# grid_bounding_box_style fixed_cube
# cube_min_coord -2.0
# cube_max_coord 10.0
should_perturb_grid 1

verbosity 2

## Run mode
run_mode fixer
measure_module_timings 1

## Scene params
scene_type two_roll
# dt 0.041
dt 0.5
max_sim_time 6.2
# run_mesher_every_n_steps 10


# ## Output options
should_output_frames                    0
output_type                                obj
output_path                             testinputs/winding_numbers/keyboard/keyboard_surface.obj

##  Algorithm Parameters
edge_deep_cell_test_depth                2
grid_vertex_resolution_method             fast_marching_method
marching_cubes_method                     8_octant_method
allow_area_increasing_t1 0
allow_t2_edge_collapse 0

value_giving_mesh_vertices in_simple_front
mesh_to_grid_interpolation_method average_in_cell

run_smoothing false

run_grid_labeler 1
run_complex_cell_detector 1
run_label_resolver 1 
run_value_transferrer 1
run_state_saver     1
run_cell_separator 1
run_multi_label_marching_cuber 1
run_mesh_upkeeper 1

run_input_mesh_consistency_tests all